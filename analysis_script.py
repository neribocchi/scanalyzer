import os
import subprocess
import json
from dotenv import load_dotenv

load_dotenv()

def run_slither_analysis(contract_path, slither_options=None):
    slither_command = f"slither --json {contract_path}"

    if slither_options:
        slither_command += " " + slither_options

    process = subprocess.Popen(slither_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()

    if stderr:
        print(f"Error al ejecutar Slither: {stderr.decode('utf-8')}")
        return None

    return json.loads(stdout.decode('utf-8'))

def explore_directory(directory_path, slither_options=None):
    analysis_results = []

    for root, _, files in os.walk(directory_path):
        for file in files:
            if file.endswith(".sol"):
                contract_path = os.path.join(root, file)
                print(f"Analizando contrato: {contract_path}")

                # Ejecutar análisis de Slither para el contrato actual con opciones adicionales
                result = run_slither_analysis(contract_path, slither_options)
                if result:
                    analysis_results.append(result)

    return analysis_results

# Leer las variables de entorno
main_directory = os.getenv("MAIN_DIRECTORY")
output_file = os.getenv("OUTPUT_FILE")
slither_options = os.getenv("SLITHER_OPTIONS")  # Variable de entorno para opciones adicionales de Slither

# Ejecutar análisis para todos los contratos en el directorio con las opciones de Slither
analysis_results = explore_directory(main_directory, slither_options)

# Guardar los resultados en un archivo JSON
with open(output_file, 'w') as file:
    json.dump(analysis_results, file, indent=2)

print(f"Análisis completado. Resultados guardados en: {output_file}")
